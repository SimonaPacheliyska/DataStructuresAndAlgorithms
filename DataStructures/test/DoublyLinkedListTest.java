package data.structures.test;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Test;

import data.structures.DoublyLinkedList;

public class DoublyLinkedListTest {

    @Test
    public void givenEmptyDoublyLinkedListWhenIsEmptyIsInvkedThenReturnTrue() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        assertTrue(ls.isEmpty());
    }

    @Test
    public void givenEmptyDoublyLinkedListWhenInsertBackIsInvokedThenItsSizeBecomes1() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        ls.insertBack(-1);
        assertEquals(1, ls.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyDoublyLinkedListWhenHeadIsInvokedThenThrowNoSuchElementException() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        ls.head();
    }

    @Test
    public void givenNonEmptyDoublyLinkedListWhenHeadIsInvokedThenReturnItsFirstElement() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        ls.insertBack(0);
        ls.insertBack(5);
        ls.insertBack(1);
        assertEquals(0, ls.head().intValue());
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyDoublyLinkedListWhenTailIsInvokedThenThrowNoSuchElementException() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        ls.tail();
    }

    @Test
    public void givenNonEmptyDoublyLinkedListWhenTailIsInvokedThenReturnItsLastElement() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        ls.insertBack(0);
        ls.insertBack(5);
        ls.insertBack(1);
        assertEquals(1, ls.tail().intValue());
    }

    @Test
    public void givenNonEmptyDoublyLinkedListWhenRemoveBackIsInvokedThenRemoveItAndReturnItsData() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        ls.insertBack(0);
        ls.insertBack(5);
        ls.insertBack(1);
        assertEquals(1, ls.removeBack().intValue());
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyDoublyLinkedListWhenRemoveBackIsInvokedThenThrowNoSuchElementException() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        ls.removeBack();
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyDoublyLinkedListWhenRemoveFrontIsInvokedThenThrowNoSuchElementException() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        ls.removeFront();
    }
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void givenEmptyDoublyLinkedListWhenRemoveAtIsInvokedThenThrowIndexOutOfBoundsException() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        ls.removeAt(0);
    }
    @Test(expected = IndexOutOfBoundsException.class)
    public void givenNonEmptyDoublyLinkedListWhenRemoveAtIsInvokedWithInvalidIndexThenThrowIndexOutOfBoundsException() {
        DoublyLinkedList<Integer> ls = new DoublyLinkedList<>();
        ls.insertBack(18);
        ls.insertBack(7);
        ls.removeAt(3);
    }

}
