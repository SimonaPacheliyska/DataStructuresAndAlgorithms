package data.structures.test;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Test;

import data.structures.MyQueue;

public class MyQueueTest {

    @Test
    public void givenEmptyQueueThenIsEmptyIsInvokedThenReturnTrue() {
        MyQueue<Object> queue = new MyQueue<>();
        assertTrue(queue.isEmpty());
    }

    @Test
    public void givenEmptyQueueWhenSizeIsInvokedThenReturn0() {
        MyQueue<Object> queue = new MyQueue<>();
        assertEquals(0, queue.size());
    }

    @Test
    public void givenQueueWhenItContainsOneElementAndSizeIsInvokedThenReturn1() {
        MyQueue<Object> queue = new MyQueue<>();
        queue.enqueue(-1);
        assertEquals(1, queue.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyQueueWhenDequeueBackIsInvokedThenThrowNoSuchElementException() {
        MyQueue<Object> queue = new MyQueue<>();
        queue.dequeue();
    }

    @Test
    public void givenNonEmptyQueueWhenDequeueBackIsInvokedThenReturnItsFirstElementAndRemoveIt() {
        MyQueue<Integer> queue = new MyQueue<>();
        queue.enqueue(5);
        queue.enqueue(121);
        queue.enqueue(0);
        assertEquals(5, queue.dequeue().intValue());
    }
    
    @Test(expected = NoSuchElementException.class)
    public void givenEmptyQueueWhenFrontIsInvokedThenThrowNoSuchElementException() {
        MyQueue<Object> queue = new MyQueue<>();
        queue.front();
    }
    
    @Test
    public void givenNonEmptyQueueWhenFrontIsInvokedThenReturnFirstElementData() {
        MyQueue<Character> queue = new MyQueue<>();
        queue.enqueue('e');
        queue.enqueue('h');
        queue.enqueue('e');
        assertEquals('e',queue.front().charValue());
    }
    @Test(expected = NoSuchElementException.class)
    public void givenEmptyQueueWhenRearIsInvokedThenThrowNoSuchElementException() {
        MyQueue<Object> queue = new MyQueue<>();
        queue.rear();
    }
    
    @Test
    public void givenNonEmptyQueueWhenRearIsInvokedThenReturnLastElementData() {
        MyQueue<Character> queue = new MyQueue<>();
        queue.enqueue('e');
        queue.enqueue('h');
        queue.enqueue('h');
        assertEquals('h',queue.rear().charValue());
    }
}
