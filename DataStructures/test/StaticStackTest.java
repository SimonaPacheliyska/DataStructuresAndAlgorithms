package data.structures.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;

import org.junit.Test;

import data.structures.StaticStack;

public class StaticStackTest {

    @Test
    public void givenEmptyStackWhenIsEmptyIsInvokedThenReturnTrue() {
        StaticStack stack = new StaticStack();
        assertTrue(stack.isEmpty());
    }

    @Test
    public void givenEmptyStackWhenSizeIsInvokedThenReturn0() {
        StaticStack stack = new StaticStack();
        assertEquals(0, stack.size());
    }

    @Test
    public void givenStackWhenItHas1ElementAndSizeIsInvokedThenReturn1() {
        StaticStack stack = new StaticStack();
        stack.push(5);
        assertEquals(1, stack.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyStackWhenPopIsInvokedThenThrowNoSuchElementException() {
        StaticStack stack = new StaticStack();
        stack.pop();
    }

    @Test
    public void givenNonEmptyStackWhenPeekIsInvokedThenReturnLastAddedElement() {
        StaticStack stack = new StaticStack();
        stack.push(4);
        stack.push(2);
        assertEquals(2, stack.peek());
    }

    @Test
    public void givenNonEmptyStackWhenPeekIsInvokedThenReturnLastAddedElementAndRemoveIt() {
        StaticStack stack = new StaticStack();
        stack.push(-1);
        stack.push(0);
        assertEquals(0, stack.pop());
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyStackWhenPeekIsInvokedThenThrowNoSuchElementException() {
        StaticStack stack = new StaticStack();
        stack.pop();
    }

}
