package data.structures.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import data.structures.DynamicArray;

public class DynamicArrayTest {

    @Test
    public void givenEmptyDynamicArrayWhenSizeIsInvokedThenReturn0() {
       DynamicArray dArray = new DynamicArray();
       assertEquals(0,dArray.size());
    }
    
    @Test
    public void givenEmptyDynamicArrayWhenElementIsAddedThenItsSizeBecomes1() {
        DynamicArray dArray = new DynamicArray();
        dArray.add(-97);
        assertEquals(1, dArray.size());
    }
}
