package data.structures.test;

import static org.junit.Assert.*;

import org.junit.Test;

import data.structures.BST;

public class BSTtest {

    @Test
    public void givenKeyWhenAddIsInvokedThenAddTheKeyAndReturnTrue() {
        BST<Integer> bTree = new BST<>();
        assertTrue(bTree.add(5));
    }
    
    @Test
    public void givenNonEmptyBinarySearchTreeWhenItContainsSpecifiedElementAndSearchIsInvokedThenReturnTrue() {
        BST<Integer> bTree = new BST<>();
        bTree.add(14);
        bTree.add(-1);
        bTree.add(18);
        assertTrue(bTree.search(-1));
    }
    

}
