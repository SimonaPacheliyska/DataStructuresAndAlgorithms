package data.structures.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;

import org.junit.Test;

import data.structures.MyLinkedList;

public class LinkedListTest {

    @Test
    public void givenEmptyLinkedListWhenIsEmptyIsInvkedThenReturnTrue() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        assertTrue(ls.isEmpty());
    }

    @Test
    public void givenEmptyLinkedListWhenInsertBackIsInvokedThenItsSizeBecomes1() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.insertBack(-1);
        assertEquals(1, ls.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyLinkedListWhenHeadIsInvokedThenThrowNoSuchElementException() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.head();
    }

    @Test
    public void givenNonEmptyLinkedListWhenHeadIsInvokedThenReturnItsFirstElement() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.insertBack(0);
        ls.insertBack(5);
        ls.insertBack(1);
        assertEquals(0, ls.head().intValue());
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyLinkedListWhenTailIsInvokedThenThrowNoSuchElementException() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.tail();
    }

    @Test
    public void givenNonEmptyLinkedListWhenTailIsInvokedThenReturnItsLastElement() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.insertBack(0);
        ls.insertBack(5);
        ls.insertBack(1);
        assertEquals(1, ls.tail().intValue());
    }

    @Test
    public void givenNonEmptyLinkedListWhenRemoveBackIsInvokedThenRemoveItAndReturnItsData() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.insertBack(0);
        ls.insertBack(5);
        ls.insertBack(1);
        assertEquals(1, ls.removeBack().intValue());
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyLinkedListWhenRemoveBackIsInvokedThenThrowNoSuchElementException() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.removeBack();
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyLinkedListWhenRemoveFrontIsInvokedThenThrowNoSuchElementException() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.removeFront();
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void givenEmptyLinkedListWhenRemoveAfterIsInvokedThenThrowIndexOutOfBoundsException() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.removeAfter(0);
    }
    @Test(expected = IndexOutOfBoundsException.class)
    public void givenNonEmptyLinkedListWhenRemoveAfterIsInvokedWithInvalidIndexThenThrowIndexOutOfBoundsException() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.insertBack(5);
        ls.insertBack(0);
        ls.insertBack(4);
        ls.removeAfter(2);
    }
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void givenEmptyLinkedListWhenRemoveBeforeIsInvokedThenThrowIndexOutOfBoundsException() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.removeBefore(0);
    }
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void givenNonEmptyLinkedListWhenRemoveBeforeIsInvokedWithInvalidIndexThenThrowIndexOutOfBoundsException() {
        MyLinkedList<Integer> ls = new MyLinkedList<>();
        ls.insertBack(5);
        ls.insertBack(0);
        ls.insertBack(4);
        ls.removeBefore(0);
    }

}
