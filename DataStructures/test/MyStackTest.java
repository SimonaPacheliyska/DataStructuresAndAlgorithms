package data.structures.test;

import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Test;

import data.structures.MyStack;

public class MyStackTest {

    @Test
    public void givenEmptyStackWhenIsEmptyIsInvokedThenReturnTrue() {
        MyStack<Object> stack = new MyStack<>();
        assertTrue(stack.isEmpty());
    }

    @Test
    public void givenEmptyStackWhenSizeIsInvokedThenReturn0() {
        MyStack<Object> stack = new MyStack<>();
        assertEquals(0, stack.size());
    }

    @Test
    public void givenStackWhenItContainsOneElementAndSizeIsInvokedThenReturn1() {
        MyStack<Object> stack = new MyStack<>();
        stack.pushBack(-1);
        assertEquals(1, stack.size());
    }

    @Test(expected = NoSuchElementException.class)
    public void givenEmptyStackWhenPopBackIsInvokedThenThrowNoSuchElementException() {
        MyStack<Object> stack = new MyStack<>();
        stack.popBack();
    }

    @Test
    public void givenNonEmptyStackWhenPopBackIsInvokedThenReturnItsLastElementAndRemoveIt() {
        MyStack<Integer> stack = new MyStack<>();
        stack.pushBack(5);
        stack.pushBack(121);
        stack.pushBack(0);
        assertEquals(0, stack.popBack().intValue());
    }
    
    @Test(expected = NoSuchElementException.class)
    public void givenEmptyStackWhenPeekIsInvokedThenThrowNoSuchElementException() {
        MyStack<Object> stack = new MyStack<>();
        stack.peek();
    }
    
    @Test
    public void givenNonEmptyStackWhenPeekIsInvokedThenReturnLastElementData() {
        MyStack<Character> stack = new MyStack<>();
        stack.pushBack('k');
        stack.pushBack('e');
        stack.pushBack('k');
        stack.pushBack('e');
        assertEquals('e',stack.peek().charValue());
    }

}
