package data.structures.algorithms;

import java.util.Random;

public class Sorting {

    private static void merge(int[] numbers, int inf, int sup, int[] buffer) {
        int leftEnd = (inf + sup) / 2;
        int rightStart = leftEnd + 1;
        int size = sup - inf + 1;

        int left = inf;
        int right = rightStart;
        int indx = inf;

        while (left <= leftEnd && right <= sup) {
            if (numbers[left] <= numbers[right]) {
                buffer[indx] = numbers[left];
                ++left;
            } else {
                buffer[indx] = numbers[right];
                ++right;
            }
            ++indx;
        }
        System.arraycopy(numbers, left, buffer, indx, leftEnd - left + 1);
        System.arraycopy(numbers, right, buffer, indx, sup - right + 1);
        System.arraycopy(buffer, inf, numbers, inf, size);
    }

    private static void mergeSortHalves(int[] numbers, int inf, int sup,
            int[] buffer) {
        if (inf >= sup) {
            return;
        }
        int mid = (inf + sup) / 2;
        mergeSortHalves(numbers, inf, mid, buffer);
        mergeSortHalves(numbers, mid + 1, sup, buffer);
        merge(numbers, inf, sup, buffer);
    }

    public static void mergeSort(int[] numbers) {
        if (numbers.length == 0 || numbers.length == 1) {
            return;
        }
        mergeSortHalves(numbers, 0, numbers.length - 1,
                new int[numbers.length]);
    }

    private static void quickSort(int[] numbers, int inf, int sup) {
        if (inf >= sup) {
            return;
        }
        Random rand = new Random();
        int pivot = numbers[inf + rand.nextInt(sup - inf)];
        int indx = partition(numbers, inf, sup, pivot);
        quickSort(numbers, inf, indx - 1);
        quickSort(numbers, indx + 1, sup);
    }

    private static int partition(int[] numbers, int inf, int sup, int pivot) {
        while (inf <= sup) {
            while (numbers[inf] < pivot) {
                ++inf;
            }
            while (numbers[sup] > pivot) {
                --sup;
            }
            if (inf <= sup) {
                int temp = numbers[inf];
                numbers[inf] = numbers[sup];
                numbers[sup] = temp;
                ++inf;
                --sup;
            }
        }
        return inf;
    }

    public static void quickSort(int[] numbers) {
        quickSort(numbers, 0, numbers.length - 1);

    }

    public void selectionSort(int[] numbers) {
        for (int i = 0; i < numbers.length - 1; ++i) {
            for (int j = i + 1; j < numbers.length; ++j) {
                if (numbers[i] > numbers[j]) {
                    numbers[i] = numbers[i] ^ numbers[j];
                    numbers[j] = numbers[i] ^ numbers[j];
                    numbers[i] = numbers[i] ^ numbers[j];
                }
            }
        }
    }

    public void bubbleSort(int[] numbers) {
        for (int i = numbers.length - 1; i > 0; --i)
            for (int j = 0; j < i; ++j) {
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
    }

}
