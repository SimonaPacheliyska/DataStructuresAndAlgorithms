package data.structures.tasks;

import data.structures.MyStack;

public class TowerOfHanoi {
    private MyStack<Integer> rod;
    private int indx;

    public TowerOfHanoi(int indx) {
        rod = new MyStack<>();
        this.indx = indx;
    }

    public void add(int elem) {
        if (!rod.isEmpty() && rod.peek() < elem) {
            throw new IllegalArgumentException("Cant place the disk");
        } else {
            rod.pushBack(elem);
        }
    }

    public void moveTo(TowerOfHanoi tower) {
        if (!rod.isEmpty()) {
            int top = this.rod.popBack().intValue();
            tower.add(top);
            System.out.println("Move disk " + top + " from Rod" + indx + " to Rod"
                    + tower.getIndx());
        }
    }

    public void moveDisks(int diskCntr, TowerOfHanoi destination,
            TowerOfHanoi auxiliary) {
        if (diskCntr > 0) {
            moveDisks(diskCntr - 1, auxiliary, destination);
            moveTo(destination);
            auxiliary.moveDisks(diskCntr - 1, destination, this);
        }
    }

    public static void moving(int disks) {
        TowerOfHanoi[] towers = new TowerOfHanoi[3];
        for (int i = 0; i < 3; i++) {
            towers[i] = new TowerOfHanoi(i);
        }
        for (int i = disks - 1; i >= 0; i--) {
            towers[0].add(i);
        }
        towers[0].moveDisks(disks, towers[2], towers[1]);
    }

    public int getIndx() {
        return this.indx;
    }

}
