package data.structures;

import java.util.NoSuchElementException;

public class MyLinkedList<T> {

    private class Node {
        T data;
        Node next;

        private Node() {
            this.data = null;
            this.next = null;
        }

        private Node(final T data, final Node next) {
            this.data = data;
            this.next = next;
        }
    }

    private Node head;
    private Node tail;
    private long size_;

    private Node nodeAt(int indx) {
        Node nodeAt = head;
        for (int i = 0; i < indx; ++i) {
            nodeAt = nodeAt.next;
        }
        return nodeAt;
    }

    public MyLinkedList() {
        head = null;
        tail = null;
        size_ = 0;
    }

    public boolean isEmpty() {
        return size_ == 0;
    }

    public long size() {
        return size_;
    }

    public T head() {
        if (head != null) {
            return head.data;
        }
        throw new NoSuchElementException("Empty list");
    }

    public T tail() {
        if (tail != null) {
            return tail.data;
        }
        throw new NoSuchElementException("Empty list");
    }

    public void insertBack(final T data) {
        if (head == null) {
            head = new Node(data, null);
            tail = head;
        } else {
            tail.next = new Node(data, null);
            tail = tail.next;
        }
        ++size_;
    }

    public T removeBack() {
        if (tail == null) {
            throw new NoSuchElementException("Empty list");
        }
        T dataToReturn = tail.data;
        Node it = head;
        while (it.next.next != null) {
            it = it.next;
        }
        it.next = null;
        tail = it;
        --size_;
        return dataToReturn;
    }

    public void insertFront(final T data) {
        if (head == null) {
            head = new Node(data, null);
            tail = head;
        } else {
            head = new Node(data, this.head);
        }
        ++size_;
    }

    public T removeFront() {
        if (head == null) {
            throw new NoSuchElementException("Empty list");
        }
        T dataToReturn = head.data;
        head = head.next;
        --size_;
        return dataToReturn;
    }

    public void insertAfter(int indx, final T data) {
        if (indx >= size_ || indx < 0) {
            throw new IndexOutOfBoundsException("Ivalid index");
        }
        Node curr = nodeAt(indx);
        curr.next = new Node(data, curr.next);
        if (indx == size_ - 1) {
            tail = curr.next;
        }
        ++size_;
    }

    public void insertBefore(int indx, final T data) {
        insertAfter(indx - 1, data);
    }

    public void removeAfter(int indx) {
        if (indx < 0 || indx >= size_ - 1) {
            throw new IndexOutOfBoundsException("Invalid index");
        }
        Node nodeAt = nodeAt(indx);
        nodeAt.next = nodeAt.next.next;
        if (nodeAt.next == null) {
            tail = nodeAt;
        }
        --size_;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        Node it = head;
        while (it != null) {
            str.append(it.data);
            str.append(" ");
            it = it.next;
        }
        return str.toString();
    }

}
