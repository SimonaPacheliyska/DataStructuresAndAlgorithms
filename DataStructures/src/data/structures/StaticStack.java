package data.structures;

import java.util.NoSuchElementException;

public class StaticStack {

    private static final int DEFAULT_CAPACITY = 16;

    private Object[] items;
    private int size;
    private int capacity;

    public StaticStack() {
        items = new Object[DEFAULT_CAPACITY];
        capacity = DEFAULT_CAPACITY;
        size = 0;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void push(Object item) {
        if (size + 1 > capacity) {
            resize(2 * capacity);
        } else {
            items[size++] = item;
        }
    }

    public Object pop() {
        if (size == 0) {
            throw new NoSuchElementException("Empty stack");
        }
        Object toReturn = items[size - 1];
        --size;
        return toReturn;
    }

    public Object peek() {
        if (size == 0) {
            throw new NoSuchElementException("Empty stack");
        }
        return items[size - 1];
    }

    private void resize(int newCapacity) {
        Object[] buffer = new Object[newCapacity];
        capacity = newCapacity;
        int i = 0;
        for (Object item : items) {
            buffer[i++] = item;
        }
        this.items = buffer;
    }
}
