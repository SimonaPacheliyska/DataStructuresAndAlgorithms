package data.structures;

import java.util.NoSuchElementException;

public class DoublyLinkedList<T> {

    private class Node {
        private T data;
        private Node prev;
        private Node next;

        private Node() {
            data = null;
            prev = null;
            next = null;
        }

        private Node(final T data, Node prev, Node next) {
            this.data = data;
            this.prev = prev;
            this.next = next;
        }
    }

    private Node head;
    private Node tail;
    private int size_;

    public DoublyLinkedList() {
        tail = null;
        head = null;
        size_ = 0;
    }

    public boolean isEmpty() {
        return size_ == 0;
    }

    public final int size() {
        return size_;
    }

    public void insertBack(final T data) {
        if (head == null) {
            head = new Node(data, null, null);
            tail = head;
        } else {
            tail.next = new Node(data, tail, null);
            tail = tail.next;
        }
        ++size_;
    }

    public void insertFront(final T data) {
        if (head == null) {
            head = new Node(data, null, null);
            tail = head;
        } else {
            Node newNode = new Node(data, null, head);
            head = head.prev = newNode;
        }
        ++size_;
    }

    public T removeBack() {
        if (tail == null) {
            throw new NoSuchElementException("Empty list");
        }
        T dataToReturn = tail.data;
        tail = tail.prev;
        tail.next = null;
        --size_;
        return dataToReturn;
    }

    public T removeFront() {
        if (head == null) {
            throw new NoSuchElementException("Empty list");
        }
        T tmpData = head.data;
        head = head.next;
        head.prev = null;
        --size_;
        return tmpData;
    }

    public T removeAt(int indx) {
        if (indx < 0 || indx > size_ - 1) {
            throw new IndexOutOfBoundsException("Invalid index");
        }
        if (indx == 0) {
            return removeFront();
        }
        if (indx == size_ - 1) {
            return removeBack();
        }
        Node prev = nodeAt(indx - 1);
        T dataToReturn = nodeAt(indx).data;
        prev.next = prev.next.next;
        if (prev.next.next != null) {
            prev.next.prev = prev;
        }
        --size_;
        return dataToReturn;
    }

    public T head() {
        if (this.head == null) {
            throw new NoSuchElementException("Empty list");
        }
        return this.head.data;
    }

    public T tail() {
        if (this.tail == null) {
            throw new NoSuchElementException("Empty list");
        }
        return tail.data;
    }

    /////////////////////////////////////////////////////////////////////////////////////////

    private Node nodeAt(int indx) {
        Node nodeAt = head;
        for (int i = 0; i < indx; ++i) {
            nodeAt = nodeAt.next;
        }
        return nodeAt;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for(Node it = head ; it!=null;it = it.next) {
            result.append(it.data);
            result.append(" ");
        }
        return result.toString();
    }
}
