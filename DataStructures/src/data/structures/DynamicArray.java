package data.structures;

public class DynamicArray {

    private final int DEFAULT_CAPACITY = 16;

    private int[] data;
    private int size;
    private int capacity;

    public DynamicArray() {
        this.data = new int[DEFAULT_CAPACITY];
        this.size = 0;
        this.capacity = DEFAULT_CAPACITY;
    }

    public DynamicArray(int capacity) {
        this.capacity = capacity;
        data = new int[capacity];
        size = 0;
    }

    public int size() {
        return size;
    }

    public void add(int elem) {
        if (size + 1 > capacity) {
            int newCapacity = capacity <= 0 ? 1 : 2 * capacity;
            resize(newCapacity);
        } else {
            data[size] = elem;
            ++size;
        }
    }

    private void resize(int newCapacity) {
        this.capacity = newCapacity;
        int[] temp = new int[capacity];
        for (int i = 0; i < size; ++i) {
            temp[i] = data[i];
        }
        data = temp;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i : data) {
            builder.append(i);
            builder.append(" ");
        }
        return builder.toString();
    }

}
