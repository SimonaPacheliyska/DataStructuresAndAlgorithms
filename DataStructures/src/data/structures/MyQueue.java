package data.structures;

import java.util.NoSuchElementException;

public class MyQueue<T> {

    private class Node {
        T data;
        Node next;

        private Node() {
            this.data = null;
            this.next = null;
        }

        private Node(final T data, final Node next) {
            this.data = data;
            this.next = next;
        }
    }

    private Node head;
    private Node tail;
    private long size_;

    public MyQueue() {
        tail = head = null;
        size_ = 0;
    }

    public boolean isEmpty() {
        return size_ == 0;
    }

    public long size() {
        return size_;
    }

    public void enqueue(final T data) {
        if (head == null) {
            tail = head = new Node(data, null);
        } else {
            tail.next = new Node(data, null);
            tail = tail.next;
        }
        ++size_;
    }

    public T dequeue() {
        if (size_ == 0) {
            throw new NoSuchElementException("Empty queue");
        }
        T dataToReturn = head.data;
        head = head.next;
        --size_;
        return dataToReturn;
    }

    public T front() {
        if (size_ == 0) {
            throw new NoSuchElementException("Empty queue");
        }
        return head.data;
    }

    public T rear() {
        if (tail == null) {
            throw new NoSuchElementException("Empty queue");
        }
        return tail.data;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Node it = head;
        while (it != null) {
            builder.append(it.data);
            builder.append(" ");
            it = it.next;
        }
        return builder.toString();
    }
}
