package data.structures;

public class BST<T extends Comparable<T>> {

    private class Node<P extends Comparable<P>> implements Comparable<Node<P>> {
        P data;
        Node<P> leftChild;
        Node<P> rightChild;

        private Node() {
            data = null;
            leftChild = null;
            rightChild = null;
        }

        private Node(final P data, final Node<P> leftChild,
                final Node<P> rightChild) {
            this.data = data;
            this.leftChild = leftChild;
            this.rightChild = rightChild;
        }

        @Override
        public int compareTo(BST<T>.Node<P> o) {
            return this.data.compareTo(o.data);
        }

    }

    private Node<T> root;

    public BST() {
        this.root = null;
    }

    public boolean add(final T key) {
        root = add(this.root, key);
        return true;
    }

    public boolean search(final T key) {
        return search(root, key);
    }

    public void printInOrder() {
        printInOrder(this.root);
    }
    
    public void remove(final T key) {
        root = remove(this.root,key);
    }

    private Node<T> add(Node<T> node, final T key) {
        if (node == null) {
            return new Node<T>(key, null, null);
        }

        if (node.data.compareTo(key) == -1) {
            node.rightChild = add(node.rightChild, key);
        } else {
            node.leftChild = add(node.leftChild, key);
        }
        return node;
    }

    private boolean search(Node<T> root, final T key) {
        if (root == null) {
            return false;
        }
        if (root.data.compareTo(key) == 0) {
            return true;
        }
        if (root.data.compareTo(key) == 1) {
            return search(root.leftChild, key);
        }
        return search(root.rightChild, key);
    }

    private void printInOrder(Node<T> node) {
        if (node != null) {
            printInOrder(node.leftChild);
            System.out.print(node.data + " ");
            printInOrder(node.rightChild);
        }
    }

    private Node<T> remove(Node<T> node, final T key) {
        if (node == null) {
            return node;
        }
        if (node.data.compareTo(key) == -1 && node.data.compareTo(key) != 0) {
            node.rightChild = remove(node.rightChild, key);
        }
        if (node.data.compareTo(key) == 1) {
            node.leftChild = remove(node.leftChild, key);
        } else {
            if (node.leftChild == null) {
                return node.rightChild;
            } else if (node.rightChild == null) {
                return node.leftChild;
            }
            node.data = getMaxIn(node.leftChild).data;
            node.leftChild = remove(node.rightChild, node.data);
        }
        return node;
    }

    private Node<T> getMaxIn(Node<T> node) {
       while(!node.rightChild.equals(null)) {
           node = node.rightChild;
       }
       return node;
    }
}