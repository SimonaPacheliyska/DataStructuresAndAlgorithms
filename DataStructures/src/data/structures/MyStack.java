package data.structures;

import java.util.NoSuchElementException;

public class MyStack<T> {

    /******************************************************************************************/
    private class Node {
        private T data;
        private Node next;

        private Node() {
            this.data = null;
            this.next = null;
        }

        private Node(final T data, final Node next) {
            this.data = data;
            this.next = next;
        }
    }

    /****************************************************************************************/

    private Node top;
    private long size_;

    public MyStack() {
        this.top = null;
        this.size_ = 0;
    }

    public boolean isEmpty() {
        return size_ == 0;
    }

    public long size() {
        return size_;
    }

    /////////////////////////////////////////////////////////////////////////////////////

    public void pushBack(final T data) {
        top = new Node(data, this.top);
        ++size_;
    }

    public T popBack() {
        if (size_ == 0) {
            throw new NoSuchElementException("Empty stack");
        }
        T dataToReturn = top.data;
        top = top.next;
        --size_;
        return dataToReturn;
    }

    public T peek() {
        if (size_ == 0) {
            throw new NoSuchElementException("Empty stack");
        }
        return top.data;
    }

    //////////////////////////////////////////////////////////////////////////////////////

   
    @Override
    public String toString() {
        StringBuilder sBuilder = new StringBuilder();
        Node it = top;
        while (it != null) {
            sBuilder.append(it.data);
            sBuilder.append(" ");
            it = it.next;
        }
        return sBuilder.toString();
    }

}
